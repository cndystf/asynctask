import express from "express";
import { checkHealth } from './controller/checkHealthController.js';
import { bio } from "./controller/biodataController.js";
import { checkAuthKey } from "./middleware/checkKey.js";
//cara pertama 
// import { get as getProduct ,create as createProduct,
//     update as updateProduct ,destroy as deleteProduct,
//     find as findProduct } from "./controller/productController.js";

//cara kedua
import productApi from './api/productApi.js';

const router = express.Router();

router.get('/check-health', checkHealth)
router.get('/biodata', checkAuthKey,bio)
router.use('/product', productApi)

// router.get('/product/list', getProduct)
// router.post('/product/create', createProduct)
// router.put('/product/update', updateProduct)
// router.delete('/product/delete', deleteProduct)
// router.post('/product/find', findProduct)

export default router

//mavers
// router.get('/',mainRoute)
// router.get('/check-health', checkHealth)
// router.get('/biodata', checkAuthKey, seeBio)