import express from "express";
import dotenv from "dotenv";
import bodyParser from "body-parser";
import cors from "cors";
import router from './router.js';
dotenv.config();

const app = express()
const port = process.env.PORT || 3500

app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
app.use('/', router)

app.listen(port, () => {
    console.log(`server is running in port ${port}`)
})


//view engine mavers
// import express from "express";
// import dotenv from 'dotenv';

// dotenv.config()
// const app = express()
// const port = process.env.PORT || 3500

// app.set('view engine', 'ejs')
// app.get("/", (req,res) => {
//     res.render('index', {
//         name: req.query.name || 'Guest'
//     })
// })

// app.get("/ver1", (req,res) => {
//     res.render('ver1', {
//         text1: req.query.text1 || 'Endpoint 1'
//     })
// })

// app.get("/ver2", (req,res) => {
//     res.render('ver2', {
//         text2: req.query.text2 || 'Endpoint 2'
//     })
// })

// app.get("/ver3", (req,res) => {
//     res.render('ver3', {
//         text3: req.query.text3 || 'Endpoint 3'
//     })
// })

// app.get("/ver4", (req,res) => {
//     res.render('ver4', {
//         text4: req.query.text4 || 'Endpoint 4'
//     })
// })

// app.get("/ver5", (req,res) => {
//     res.render('ver5', {
//         text5: req.query.text5 || 'Endpoint 5'
//     })
// })

// app.listen(port, () => { //klo bener maka akan menjalankan ini
//     console.log(`server is running on port ${port}`)
// })